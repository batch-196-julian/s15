console.log("Hello World");

let firstName = 'Josue';
let lastName = 'Julian';
let age = 24;

let hobbies = ['Basketball', 'Motrocycle Rides', 'Hiking'];

let workAddress = {
	houseNumber: 6568,
	street: "Rambutan St.",
	state: "America"

} 

console.log('First Name: ' + firstName );
console.log('Last Name: ' + lastName );
console.log("Hobbies: ")
console.log(hobbies);
console.log("Work Addres:")
console.log(workAddress);

/*
	1. Create variables to store to the following user details:

	-first name - String

	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object



		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let ages = 40;
	console.log("My current age is:");
	console.log(ages);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		username: "captain_america",
		fullNaMe: "Steve Rogers",
		ages: 40,
		isActive: false,
}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullNames = "Bucky Barnes";
	console.log("My bestfriend is: " + fullNames);

	const lastLocation = "Arctic Ocean";
	const lastLocations = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocations);